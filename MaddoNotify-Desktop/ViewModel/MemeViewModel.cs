﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Threading;
using MaddoLibrary.WPF.NET46.Commands;
using MaddoNotify_Desktop.Config;
using MaddoParticleLibrary;

namespace MaddoNotify_Desktop.ViewModel
{
    public class MemeViewModel : ViewModelBase
    {


        private string _userName;
        public string UserName
        {
            get => MaddoSettings.Instance.UserName;
            //set { _userName = value; RaisePropertyChanged(); }
            set { MaddoSettings.Instance.UserName = value; RaisePropertyChanged(); }
        }

        private string _password;
        public string Password
        {
            //get => _password;
            get => MaddoSettings.Instance.Password;
            //set { _password = value; RaisePropertyChanged(); }
            set { MaddoSettings.Instance.Password = value; RaisePropertyChanged(); }
        }

        private string _token;

        public string Token
        {
            get => MaddoSettings.Instance.Token;
            set { MaddoSettings.Instance.Token = value; RaisePropertyChanged(); }
        }

        private ObservableCollection<string> _log = new ObservableCollection<string>();

        public ObservableCollection<string> Log
        {
            get => _log;
            set { _log = value; RaisePropertyChanged(); }
        }

        private bool _remember;

        public bool Remember
        {
            //get => _remember;
            get => MaddoSettings.Instance.Remember;
            //set { _remember = value; RaisePropertyChanged(); }
            set { MaddoSettings.Instance.Remember = value; RaisePropertyChanged(); }
        }

        private void LoadSettings()
        {
            MaddoSettings.Instance.Load();
            //MaddoSettings.Instance.Save();

            //Remember = Properties.Settings.Default.Remember;
            //if (Remember)
            //{
            //    UserName = Properties.Settings.Default.UserName;
            //    Password = Properties.Settings.Default.Password;
            //}

        }

        private bool _loggedIn = false;

        public bool LoggedIn
        {
            get { return _loggedIn; }
            set { _loggedIn = value; RaisePropertyChanged(); }
        }

        public RelayCommand IconClickCallback { get; private set; }
        public RelayCommand OpenAccountSettingsCommand { get; set; }
        public RelayCommand OpenEventsEditorCommand { get; set; }
        public AwaitableDelegateCommand LoginCommand { get; private set; }
        public RelayCommand LogoutCommand { get; private set; }

        public MemeViewModel()
        {
            LoadSettings();
            RegisterCommands();
        }

        private void RegisterCommands()
        {
            IconClickCallback = new RelayCommand(ExecuteIconClick);
            LoginCommand = new AwaitableDelegateCommand(Login, () => !LoggedIn);

            LogoutCommand = new RelayCommand(Logout, () => LoggedIn);
        }

        private void Logout()
        {
            //ParticleCloud.SharedCloud.Logout();
            MaddoParticleLibrary.MaddoParticle.Logout();
            LogOnUi($"{DateTime.Now} Logged Out");
            LoggedIn = false;
        }

        public void LogOnUi(string text)
        {
            DispatcherHelper.CheckBeginInvokeOnUI(
                () =>
                {
                    Log.Add(text);

                });
        }

        //private async Task Login()
        //{
        //    var success = await InnerLogin();
        //    if (!success)
        //    {
        //        return;
        //    }
        //    LoggedIn = true;

        //    SaveSettings();
        //    Log.Add($"Access Token: {ParticleCloud.SharedCloud.AccessToken}");


        //    await ParticleCloud.SharedCloud.SubscribeToAllEventsWithPrefixAsync(MotionSensorHandler, "MotionSensor");
        //    await ParticleCloud.SharedCloud.SubscribeToAllEventsWithPrefixAsync(CampanelloHandler, "Campanello");
        //}

        private async Task Login()
        {

            //var settings = MaddoParticleLibrary.Settings.ReadSettings();
           
            var settings = Settings.CreateManualSettings(UserName, Password, Token);

            MaddoSettings.Instance.LoadEvents();
            var events = MaddoSettings.Instance.Events; //MaddoParticleLibrary.Events.Read();
            try
            {

                string token = await MaddoParticleLibrary.MaddoParticle.Start(settings, events, output =>
                {
                    LogOnUi(output);

                }, (eventData) =>
                {
                    //MaddoParticleLibrary.FcmStuff.Notify(eventData, output => { Console.WriteLine(output); }).GetAwaiter().GetResult();
                    DispatcherHelper.CheckBeginInvokeOnUI(
                    () =>
                    {
                        //Log.Add(name);
                        NotificationManager.Notify("MaddoNotify", $"{DateTime.Now} - {eventData.Comment}. Data: {eventData.StringData} ");
                    });

                    
                });
                LoggedIn = true;

                if (!string.IsNullOrWhiteSpace(token))
                {
                    Token = token;
                    //settings.Token = token;
                    
                }
                MaddoSettings.Instance.Save();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }


        //private void CampanelloHandler(object sender, ParticleEventResponse particleEvent)
        //{
        //    LogEvent(particleEvent.Name);
        //}

        //private void MotionSensorHandler(object sender, ParticleEventResponse particleEvent)
        //{


        //    LogEvent(particleEvent.Name);

        //}

        private void LogEvent(string name)
        {
            DispatcherHelper.CheckBeginInvokeOnUI(
                () =>
                {
                    Log.Add(name);
                    NotificationManager.Notify("MaddoNotify", $"{DateTime.Now} - {name} ");
                });

            //Dispatcher.CurrentDispatcher.BeginInvoke(new Action(delegate ()
            //{

            //}));
        }

        private void SaveSettings()
        {
            MaddoSettings.Instance.Save();

            //if (Remember)
            //{
            //    Properties.Settings.Default.UserName = UserName;
            //    Properties.Settings.Default.Password = Password;
            //}
            //Properties.Settings.Default.Remember = Remember;

            //Properties.Settings.Default.Save();
        }

        //private async Task<bool> InnerLogin()
        //{
        //    if (_loggedIn) return false;
        //    if (string.IsNullOrWhiteSpace(UserName) || string.IsNullOrWhiteSpace(Password))
        //    {
        //        //MessageBox.Show("No login name or password set");
        //        return false;
        //    }

        //    return await Particle.SDK.ParticleCloud.SharedCloud.LoginAsync(UserName, Password);
        //}

        //private void OpenAccountSettings()
        //{
        //    MessengerInstance.Send<NotificationMessage>(new NotificationMessage("OpenAccount"));
        //}

        private void ExecuteIconClick()
        {
            Log.Add("Clicked icon");
        }
    }
}
