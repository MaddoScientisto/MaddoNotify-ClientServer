﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MaddoLibrary.Helpers;
using MaddoParticleLibrary;
using Newtonsoft.Json;

namespace MaddoNotify_Desktop.Config
{
    public class MaddoSettings
    {
        private static MaddoSettings _instance = new MaddoSettings();

        public static MaddoSettings Instance
        {
            get { return _instance; }
        }

        private Dictionary<string, object> _settingsDict = new Dictionary<string, object>();

        private IEnumerable<Events> _eventsList;

        public void Save()
        {
            if (!Remember)
            {
                SetString("UserName", "");
                SetString("Password", "");
            }

            string settings = SerializeSettings();

            WriteFile(SettingsFilePath, settings);

           



        }

        public void SaveEvents()
        {
            WriteFile(EventsFilePath, JsonConvert.SerializeObject(_eventsList));
        }

        private void WriteFile(string path, string data)
        {
            using (StreamWriter s = new StreamWriter(path, false))
            {
                s.WriteLine(data);
            }
        }

        public static readonly string SettingsFilePath = "Config/Settings.json";
        public static readonly string DefaultSettingsFilePath = "Config/Defaultsettings.json";
        public static readonly string DefaultEventsPath = "Config/DefaultEvents.json";
        public static readonly string EventsFilePath = "Config/Events.json";

        public void Load()
        {


            if (File.Exists(SettingsFilePath))
            {
                DeserializeSettings(ReadFile(SettingsFilePath));
            }
            else
            {
                DeserializeSettings(ReadFile(DefaultSettingsFilePath));
            }

            
            LoadEvents();
        }

        public void LoadEvents()
        {
            if (File.Exists(EventsFilePath))
            {
                _eventsList = JsonConvert.DeserializeObject<IEnumerable<Events>>(ReadFile(EventsFilePath));
            }
            else
            {
                _eventsList = GetDefaultEvents();
            }
        }

        private string ReadFile(string path)
        {
            using (StreamReader s = new StreamReader(path))
            {
                return s.ReadToEnd();
            }
        }


        private IEnumerable<Events> GetDefaultEvents()
        {
            string data = ReadFile(DefaultEventsPath);
            var events = JsonConvert.DeserializeObject<IEnumerable<Events>>(data);
            return events;

        }

        public string SerializeSettings()
        {
            return JsonConvert.SerializeObject(_settingsDict);
        }

        public void DeserializeSettings(string serializedData)
        {
            _settingsDict = JsonConvert.DeserializeObject<Dictionary<string, object>>(serializedData);
        }

        public void SetBase(string key, object value)
        {
            if (_settingsDict.ContainsKey(key))
            {
                _settingsDict[key] = value;
            }
            else
            {
                _settingsDict.Add(key, value);
            }
        }

        public void SetString(string key, string value)
        {
            SetBase(key, value);
        }

        public void SetInt(string key, int value)
        {
            SetBase(key, value);
        }

        public void SetBool(string key, bool value)
        {
            SetBase(key, value);
        }

        public void SetDouble(string key, double value)
        {
            SetBase(key, value);
        }

        public void SetFloat(string key, float value)
        {
            SetBase(key, value);
        }

        public void Set<T>(string key, T value)
        {
            SetBase(key, value);
        }

        public bool Exists(string key)
        {
            return _settingsDict.ContainsKey(key);
        }

        public int GetInt(string key, int defaultValue = 0)
        {
            if (!_settingsDict.ContainsKey(key))
            {
                SetInt(key, defaultValue);
            }

            if (_settingsDict[key] is int) return (int)_settingsDict[key];
            Debug.WriteLine($"Error while parsing {key}");
            //return defaultValue;

            int r;

            if (int.TryParse(_settingsDict[key].ToString(), out r))
            {
                SetInt(key, r);

            }
            else
            {
                SetInt(key, defaultValue);
            }
            return (int)_settingsDict[key];
            //return (int) _settingsDict[key];

            //return _settingsDict.ContainsKey(key) ? (int)_settingsDict[key] : defaultValue;
        }

        public double GetDouble(string key, double defaultValue = 0)
        {
            if (!_settingsDict.ContainsKey(key))
            {
                SetDouble(key, defaultValue);
                // setdouble default
            }

            if (_settingsDict[key] is double) return (double)_settingsDict[key];
            Debug.WriteLine($"Error while parsing {key}");

            double d;
            if (double.TryParse(_settingsDict[key].ToString(), out d))
            {
                SetDouble(key, d);
                // setdouble key r
            }
            else
            {
                SetDouble(key, defaultValue);
                //setdouble defaultvalue
            }
            return (double)_settingsDict[key];
        }

        public float GetFloat(string key, float defaultValue = 0)
        {
            if (!_settingsDict.ContainsKey(key))
            {
                SetFloat(key, defaultValue);
            }

            if (_settingsDict[key] is float f) return f;
            //MaddoLogger.LogError("Error while parsing {0}", key);

            float fl = 0;
            SetFloat(key, float.TryParse(_settingsDict[key].ToString(), out f) ? fl : defaultValue);
            return (float)_settingsDict[key];
        }

        public T Get<T>(string key, T defaultValue)
        {
            if (!_settingsDict.ContainsKey(key))
            {
                Set<T>(key, defaultValue);
                // setdouble default
            }

            return (T)_settingsDict[key];
        }


        public string GetString(string key, string defaultValue = "")
        {
            if (!_settingsDict.ContainsKey(key))
            {
                SetString(key, defaultValue);
            }
            return (string)_settingsDict[key];

            //return _settingsDict.ContainsKey(key) ? (string)_settingsDict[key] : defaultValue;
        }

        public bool GetBool(string key, bool defaultValue = false)
        {
            if (!_settingsDict.ContainsKey(key))
            {
                SetBool(key, defaultValue);
                return defaultValue;
            }
            return (bool)_settingsDict[key];

            //return _settingsDict.ContainsKey(key) && (bool)_settingsDict[key];
        }


        public object GetObject(string key, object defaultValue = null)
        {
            if (!_settingsDict.ContainsKey(key))
            {
                Set(key, defaultValue);
                return defaultValue;
            }
            return _settingsDict[key];

            //return _settingsDict.ContainsKey(key) ? _settingsDict[key] : defaultValue;
        }



        public T GetEnum<T>(string name, T defaultValue)
        {
            return (T)Enum.Parse(typeof(T), GetString(name, defaultValue.ToString()));
        }

        public string UserName
        {
            get => GetString("UserName", "");
            set => SetString("UserName", value);
        }

        public string Password
        {
            get => GetString("Password", "");
            set => SetString("Password", value);
        }

        public bool Remember
        {
            get => GetBool("Remember", false);
            set => SetBool("Remember", true);
        }

        public string Token
        {
            get => GetString("Token", "");
            set => SetString("Token", value);
        }

        public IEnumerable<Events> Events
        {
            get => _eventsList;
            set => _eventsList = value;
        }


    }
}
