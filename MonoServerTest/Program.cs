﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonoServer;

namespace MonoServerTest
{
    class Program
    {
        static void Main(string[] args)
        {
            MonoServer.ParticleService s = new ParticleService();
            s.Test(args);
            Console.ReadLine();
        }
    }
}
