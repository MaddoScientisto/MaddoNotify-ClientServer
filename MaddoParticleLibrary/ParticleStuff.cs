﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Particle.SDK;

namespace MaddoParticleLibrary
{
    public class ParticleStuff
    {
        public static async Task<bool> Login(Settings settings, Action<string> outputCallback)
        {
            //Console.WriteLine("Attempting login...");
            outputCallback("Attempting login...");
            bool succ = false;
            try
            {
                if (string.IsNullOrWhiteSpace(settings.Token))
                {
                    outputCallback($"With Account {settings.UserName}.");
                    succ = await ParticleCloud.SharedCloud.LoginAsync(settings.UserName, settings.Password);
                }
                else
                {
                    outputCallback($"With token {settings.Token}.");
                    succ = await ParticleCloud.SharedCloud.TokenLoginAsync(settings.Token);
                    // try with account if token fails
                    if (!succ)
                    {
                        outputCallback("Token failed. Retrying login with account.");
                        succ = await ParticleCloud.SharedCloud.LoginAsync(settings.UserName, settings.Password);
                    }
                }


                if (succ)
                {
                    outputCallback($"Success! Access token: {ParticleCloud.SharedCloud.AccessToken}");
                    settings.Token = ParticleCloud.SharedCloud.AccessToken;
                }
            }
            catch (Exception e)
            {
                outputCallback(e.Message);
#if DEBUG
                throw;
#endif
            }


            return succ;
        }


    }
}
