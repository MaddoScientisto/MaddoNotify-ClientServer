﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FcmSharp;
using FcmSharp.Model.Options;
using FcmSharp.Model.Topics;
using FcmSharp.Requests.Topics;
using FcmSharp.Settings;


namespace MaddoParticleLibrary
{
    public class FcmStuff
    {
        public static async Task Notify(MaddoParticleLibrary.Events eventData, Action<string> resultCallback)
        {
            // Read the API Key from a File, which is not under Version Control:
            var settings = new FileBasedFcmClientSettings("config/api.key");

            // Construct the Client:
            using (var client = new FcmClient(settings))
            {

                // Construct the Data Payload to send:
                var data = new
                {
                    content = eventData.Comment,
                    type = eventData.Type,
                    soundname = eventData.Soundname,
                    extraData = eventData.StringData

                };



                // Options for the Message:
                var options = FcmMessageOptions.Builder()
                    .setTimeToLive(TimeSpan.FromDays(1))
                    .Build();

                // The Message should be sent to the News Topic:
                var message = new TopicUnicastMessage<dynamic>(options, new Topic(eventData.Topic), data);

                // Finally send the Message and wait for the Result:
                CancellationTokenSource cts = new CancellationTokenSource();

                // Send the Message and wait synchronously:
                var result = await client.SendAsync(message, cts.Token);//.GetAwaiter().GetResult();

                // Print the Result to the Console:
                resultCallback(result.ToString());
                //System.Console.WriteLine("Result = {0}", result);
            }
        }
        [Obsolete]
        public static async Task Notify(string type)
        {
            // Read the API Key from a File, which is not under Version Control:
            var settings = new FileBasedFcmClientSettings("config/api.key");

            // Construct the Client:
            using (var client = new FcmClient(settings))
            {
                string messageType = string.Empty;
                string c = string.Empty;
                if (type == "MotionSensor")
                {
                    c = "Movement";
                    messageType = "msgalert";
                }
                else if (type == "Campanello")
                {
                    c = "Doorbell";
                    messageType = "doorbell";
                }
                // Construct the Data Payload to send:
                var data = new
                {
                    content = c,
                    soundname = messageType,

                };



                // Options for the Message:
                var options = FcmMessageOptions.Builder()
                    .setTimeToLive(TimeSpan.FromDays(1))
                    .Build();

                // The Message should be sent to the News Topic:
                var message = new TopicUnicastMessage<dynamic>(options, new Topic(type), data);

                // Finally send the Message and wait for the Result:
                CancellationTokenSource cts = new CancellationTokenSource();

                // Send the Message and wait synchronously:
                var result = await client.SendAsync(message, cts.Token);//.GetAwaiter().GetResult();

                // Print the Result to the Console:
                System.Console.WriteLine("Result = {0}", result);
            }
        }
    }
}
