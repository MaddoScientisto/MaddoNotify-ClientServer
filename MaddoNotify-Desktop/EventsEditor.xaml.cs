﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MaddoNotify_Desktop.ViewModel;

namespace MaddoNotify_Desktop
{
    /// <summary>
    /// Interaction logic for EventsEditor.xaml
    /// </summary>
    public partial class EventsEditor : Window
    {
        private EventsEditorViewModel _viewModel;
        public EventsEditor()
        {
            _viewModel = new EventsEditorViewModel();
            this.DataContext = _viewModel;
            InitializeComponent();


        }

        private void EventsEditor_OnClosed(object sender, EventArgs e)
        {
            _viewModel.SaveEventsCommand.Execute(null);
        }
    }
}
