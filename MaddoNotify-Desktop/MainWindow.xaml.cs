﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GalaSoft.MvvmLight.CommandWpf;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Threading;
using MaddoNotify_Desktop.Config;
using MaddoNotify_Desktop.ViewModel;
using ToastNotifications.Messages;

namespace MaddoNotify_Desktop
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private MemeViewModel _viewModel;

        public MainWindow()
        {
            DispatcherHelper.Initialize();
            InitializeComponent();

            _viewModel = new MemeViewModel();
            DataContext = _viewModel;
            NotificationManager.SetIcon(_viewModel.IconClickCallback);
            _viewModel.OpenAccountSettingsCommand = new RelayCommand(OpenAccountSettings);

            _viewModel.OpenEventsEditorCommand = new RelayCommand(OpenEventsEditor);

        }

        private void OpenEventsEditor()
        {
            EventsEditor editor = new EventsEditor();
            editor.ShowDialog();
        }

        private void OpenAccountSettings()
        {
            AccountSetup window = new AccountSetup(_viewModel);
            window.ShowDialog();
        }

        private void MainWindow_OnClosed(object sender, EventArgs e)
        {
            MaddoSettings.Instance.Save();
        }
    }
}
