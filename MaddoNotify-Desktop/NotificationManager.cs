﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DesktopToast;
using GalaSoft.MvvmLight.CommandWpf;
using MaddoNotify_Desktop.Properties;
using NotificationsExtensions;
using ToastNotifications;
using ToastNotifications.Lifetime;
using ToastNotifications.Position;
using NotificationsExtensions.Toasts;
using Application = System.Windows.Application;
using ToastAudio = NotificationsExtensions.Toasts.ToastAudio;

namespace MaddoNotify_Desktop
{
    public static class NotificationManager
    {
        public static Notifier NotifierInstance = new Notifier(cfg =>
        {
            cfg.PositionProvider = new WindowPositionProvider(
                parentWindow: Application.Current.MainWindow,
                corner: Corner.TopRight,
                offsetX: 10,
                offsetY: 10);

            cfg.LifetimeSupervisor = new TimeAndCountBasedLifetimeSupervisor(
                notificationLifetime: TimeSpan.FromSeconds(3),
                maximumNotificationCount: MaximumNotificationCount.FromCount(5));

            cfg.Dispatcher = Application.Current.Dispatcher;
        });

        public static NotifyIcon TrayIcon;

        public static void SetIcon(RelayCommand clickEvent)
        {
            TrayIcon = new System.Windows.Forms.NotifyIcon()
            {
                Icon = System.Drawing.Icon.ExtractAssociatedIcon(Assembly.GetExecutingAssembly().Location),
                Visible = true
            };
            //ni.ShowBalloonTip(2, "ayy lmao", "top kek", ToolTipIcon.Info);
            TrayIcon.DoubleClick += (sender, args) => { clickEvent.Execute(null); };
            //ni.Icon = new System.Drawing.Icon("Main.ico");
            //ni.Visible = true;
        }

        public static void Notify(string title, string text)
        { 
            TrayIcon.ShowBalloonTip(2, title, text, ToolTipIcon.Info);


        }

        public static ToastContent Toast = new ToastContent()
        {
            Launch = "lei",

            Visual = new ToastVisual
            {
                BindingGeneric = new ToastBindingGeneric()
                {
                    AppLogoOverride = new ToastGenericAppLogo
                    {
                        HintCrop = ToastGenericAppLogoCrop.Circle,
                        Source = "http://messageme.com/lei/profile.jpg"
                    },
                    Children =
                    {
                        new AdaptiveText {Text = "New message from Lei" },
                        new AdaptiveText {Text = "NotificationsExtensions is great!" }
                    },
                    Attribution = new ToastGenericAttributionText
                    {
                        Text = "Alert"
                    },
                }
            },
            Actions = new ToastActionsCustom()
            {
                Inputs =
                {
                    new ToastTextBox("tbReply")
                    {
                        PlaceholderContent = "Type a response"
                    }
                },

                Buttons =
                {
                    new ToastButton("reply", "reply")
                    {
                        ActivationType = ToastActivationType.Background,
                        ImageUri = "Assets/QuickReply.png",
                        TextBoxId = "tbReply"
                    }
                }
            },

            Audio = new ToastAudio()
            {
                Src = new Uri("ms-winsoundevent:Notification.IM")
            }
        };
    }
}
