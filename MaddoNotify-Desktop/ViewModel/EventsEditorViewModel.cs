﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using MaddoNotify_Desktop.Config;
using MaddoNotify_Desktop.Data;
using MaddoParticleLibrary;

namespace MaddoNotify_Desktop.ViewModel
{
    public class EventsEditorViewModel : ViewModelBase
    {
        private ObservableCollection<ObservableEvent> _eventsList = new ObservableCollection<ObservableEvent>();
        public ObservableCollection<ObservableEvent> EventsList
        {
            get => _eventsList;
            set
            {
                _eventsList = value; RaisePropertyChanged();
            }
        }

        private ObservableEvent _selectedEvent;

        public ObservableEvent SelectedEvent
        {
            get => _selectedEvent;
            set { _selectedEvent = value; RaisePropertyChanged(); }
        }

        public EventsEditorViewModel()
        {
            LoadSettings();
            SaveEventsCommand = new RelayCommand(SaveEvents);
            AddNewEventCommand = new RelayCommand(AddNewEvent);
            RemoveEventCommand = new RelayCommand(RemoveEvent);
        }

        private void RemoveEvent()
        {
            _eventsList.Remove(_selectedEvent);
        }

        private void AddNewEvent()
        {
            var newEvent = new ObservableEvent(new Events() { EventName = "New Event" });
            EventsList.Add(newEvent);
            SelectedEvent = newEvent;
        }

        private void SaveEvents()
        {
            var l = (from ev in _eventsList select ev.EventData);
            MaddoSettings.Instance.Events = l;
            MaddoSettings.Instance.SaveEvents();
        }

        public RelayCommand SaveEventsCommand { get; private set; }
        public RelayCommand AddNewEventCommand { get; private set; }
        public RelayCommand RemoveEventCommand { get; private set; }

        private void LoadSettings()
        {
            var events = MaddoSettings.Instance.Events;
            foreach (var e in events)
            {
                var oe = new ObservableEvent(e);
                _eventsList.Add(oe);
            }

            //_eventsList = new ObservableCollection<Events>(MaddoSettings.Instance.Events);
            //Remember = Properties.Settings.Default.Remember;
            //if (Remember)
            //{
            //    UserName = Properties.Settings.Default.UserName;
            //    Password = Properties.Settings.Default.Password;
            //}

        }
    }
}
