﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using MaddoParticleLibrary;

//using Particle.SDK;
//using Particle.SDK.Models;

namespace MonoServer
{
    public class ParticleService : ServiceBase
    {
#if DEBUG
        // This method is for debugging of OnStart() method only.
        // Switch to Debug config, set a breakpoint here and a breakpoint in OnStart()
        public static void Main(String[] args)
        {
            (new ParticleService()).OnStart(new string[1]);
            //ServiceBase.Run(new ParticleService());
            Console.ReadLine();
        }
#endif

        public void Test(string[] args)
        {
            OnStart(args);
        }

        protected override void OnStart(string[] args)
        {
            Console.WriteLine("Starting");

            var settings = MaddoParticleLibrary.Settings.ReadSettings();

            var events = MaddoParticleLibrary.Events.Read();

            Task.Run(async () =>
            {
                string token = await MaddoParticleLibrary.MaddoParticle.Start(settings, events, output =>
                {
                    Console.WriteLine(output);
                }, (eventData) =>
                {
                    MaddoParticleLibrary.FcmStuff.Notify(eventData, output => { Console.WriteLine(output); }).GetAwaiter().GetResult();
                });
                if (!string.IsNullOrWhiteSpace(token))
                {
                    settings.Token = token;
                    Settings.WriteSettings(settings);
                }

            }).GetAwaiter().GetResult();



            //var settings = Settings.ReadSettings();

            //var events = MonoServer.Events.Read();

            //Task.Run(async () =>
            //{
            //    var succ = await ParticleStuff.Login(settings.UserName, settings.Password);
            //    if (succ)
            //    {

            //        foreach (var e in events)
            //        {
            //            Console.WriteLine($"Subscrived to event: {e.EventName}");
            //            await ParticleCloud.SharedCloud.SubscribeToAllEventsWithPrefixAsync((s, ev) =>
            //            {
            //                LogEvent(e, ev);
            //            }, e.EventName);
            //        }
            //        //await ParticleCloud.SharedCloud.SubscribeToAllEventsWithPrefixAsync(MotionSensorHandler, "MotionSensor");
            //        //await ParticleCloud.SharedCloud.SubscribeToAllEventsWithPrefixAsync(CampanelloHandler, "Campanello");
            //    }




            //    // Do any async anything you need here without worry
            //}).GetAwaiter().GetResult();


        }

        //private void CampanelloHandler(object sender, ParticleEventResponse particleEvent)
        //{
        //    LogEvent(particleEvent.Name);
        //}

        //private void MotionSensorHandler(object sender, ParticleEventResponse particleEvent)
        //{


        //    LogEvent(particleEvent.Name);

        //}

        //        private void LogEvent(MonoServer.Events currentEvent, ParticleEventResponse eventResponse)
        //        {
        //            Console.WriteLine($"{DateTime.Now} - Received Event: {eventResponse.Name}");
        //            try
        //            {
        //                FcmStuff.Notify(currentEvent).GetAwaiter().GetResult();
        //            }
        //            catch (Exception e)
        //            {
        //                Console.WriteLine(e);
        //#if DEBUG
        //                throw;
        //#endif
        //            }
        //        }

        //[Obsolete]
        //private void LogEvent(string name)
        //{
        //    Console.WriteLine($"{DateTime.Now} - Event: {name}");

        //    try
        //    {
        //        FcmStuff.Notify(name).GetAwaiter().GetResult();
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e);
        //        //throw;
        //    }


        //    //DispatcherHelper.CheckBeginInvokeOnUI(
        //    //    () =>
        //    //    {
        //    //        Log.Add(name);
        //    //        NotificationManager.Notify("MaddoNotify", $"{DateTime.Now} - {name} ");
        //    //    });

        //    //Dispatcher.CurrentDispatcher.BeginInvoke(new Action(delegate ()
        //    //{

        //    //}));
        //}


    }
}
