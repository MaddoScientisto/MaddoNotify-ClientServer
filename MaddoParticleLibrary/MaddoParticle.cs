﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Particle.SDK;
using Particle.SDK.Models;

namespace MaddoParticleLibrary
{
    public class MaddoParticle
    {
        private static MaddoParticle _instance;

        private MaddoParticle() { }

        public static MaddoParticle Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new MaddoParticle();
                }
                return _instance;
            }
        }

        private List<Guid> _events = new List<Guid>();

        public static async Task<string> Start(Settings settings, IEnumerable<MaddoParticleLibrary.Events> events,
            Action<string> outputCallback, Action<MaddoParticleLibrary.Events> notifyCallback)
        {
            return await MaddoParticle.Instance.InnerStart(settings, events, outputCallback, notifyCallback);
        }

        private async Task<string> InnerStart(Settings settings, IEnumerable<MaddoParticleLibrary.Events> events, Action<string> outputCallback, Action<MaddoParticleLibrary.Events> notifyCallback)
        {
            if (_events.Any())
            {
                Logout();
            }

            bool succ = await ParticleStuff.Login(settings, outputCallback);


            if (succ)
            {

                foreach (var e in events)
                {
                    if (!e.Enabled)
                    {
                        outputCallback($"Skipped disabled event: {e.EventName}");
                        continue;
                    }
                    outputCallback($"Subscrived to event: {e.EventName}");
                    //Console.WriteLine($"Subscrived to event: {e.EventName}");
                    //ParticleEventHandler p = delegate (object sender,
                    //    ParticleEventResponse ev)
                    //{

                    //    LogEvent(e, ev, outputCallback, notifyCallback);
                    //};

                    //_events.Add(p);

                    //await ParticleCloud.SharedCloud.SubscribeToAllEventsWithPrefixAsync(p, e.EventName);

                    var eId = await ParticleCloud.SharedCloud.SubscribeToAllEventsWithPrefixAsync((s, ev) =>
                    {
                        LogEvent(e, ev, outputCallback, notifyCallback);
                    }, e.EventName);
                    _events.Add(eId);

                }

                return settings.Token;
            }
            else
            {
                outputCallback($"Could not login.");
                return string.Empty;

            }


        }

        public static void Logout()
        {
            MaddoParticle.Instance.InnerLogout();
        }

        public void InnerLogout()
        {
            foreach (var e in _events)
            {
                ParticleCloud.SharedCloud.UnsubscribeFromEvent(e);
            }
            _events = new List<Guid>();

            ParticleCloud.SharedCloud.Logout();
        }

        private static void LogEvent(MaddoParticleLibrary.Events currentEvent, ParticleEventResponse eventResponse,
            Action<string> outputCallback, Action<MaddoParticleLibrary.Events> notifyCallback)
        {
            MaddoParticle.Instance.InnerLogEvent(currentEvent, eventResponse, outputCallback, notifyCallback);
        }

        private void InnerLogEvent(MaddoParticleLibrary.Events currentEvent, ParticleEventResponse eventResponse, Action<string> outputCallback, Action<MaddoParticleLibrary.Events> notifyCallback)
        {
            outputCallback($"{DateTime.Now} - Received Event: {eventResponse.Name} with data: {eventResponse.Data} from Device: {eventResponse.DeviceId} published at: {eventResponse.PublishedAt}");
            try
            {
                var newEvent = new Events(currentEvent) { StringData = eventResponse.Data };

                notifyCallback(newEvent);

                //FcmStuff.Notify(currentEvent, outputCallback).GetAwaiter().GetResult();
            }
            catch (Exception e)
            {
                outputCallback(e.Message);
#if DEBUG
                throw;
#endif
            }
        }
    }
}
