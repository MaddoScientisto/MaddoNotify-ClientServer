﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace MonoServer
{
    class Program
    {
#if (DEBUG != true)
        static void Main(string[] args)
        {
        ServiceBase[] ServicesToRun;
        ServicesToRun = new ServiceBase[] { new ParticleService() };
        ServiceBase.Run(ServicesToRun);
        }
#endif
    }
}
