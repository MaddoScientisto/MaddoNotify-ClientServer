﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using MaddoParticleLibrary;

namespace MaddoNotify_Desktop.Data
{
    public class ObservableEvent : ViewModelBase
    {
        private Events _eventData;

        public Events EventData => _eventData;

        public string EventName
        {
            get => _eventData.EventName;
            set { _eventData.EventName = value; RaisePropertyChanged(); }
        }

        public string Comment
        {
            get => _eventData.Comment;
            set { _eventData.Comment = value; RaisePropertyChanged(); }
        }

        public string SoundName
        {
            get => _eventData.Soundname;
            set { _eventData.Soundname = value; RaisePropertyChanged(); }
        }

        public string Topic
        {
            get => _eventData.Topic;
            set { _eventData.Topic = value; RaisePropertyChanged(); }
        }


        public bool Enabled
        {
            get => _eventData.Enabled;
            set { _eventData.Enabled = value; RaisePropertyChanged(); }
        }
        public ObservableEvent(Events eventData)
        {
            _eventData = eventData;
        }
    }
}
