﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MaddoParticleLibrary
{
    [Serializable]
    public class Settings
    {
        public string UserName;
        public string Password;
        public string Token;


        public static Settings CreateManualSettings(string userName, string password, string token = "")
        {
            Settings s = new Settings
            {
                UserName = userName,
                Password = password,
                Token = token
            };
            return s;
        }

        public static Settings ReadSettings()
        {
            return JsonUtility.ReadObject<Settings>("config/settings.json");
        }

        public static void WriteSettings(Settings settings)
        {
            JsonUtility.WriteFile("config/settings.json", JsonConvert.SerializeObject(settings));
        }
    }

    [Serializable]
    public class Events
    {
        public string EventName;
        public string Comment;
        public string Soundname;
        public string Topic;
        public string Type;
        public bool Enabled;
        [NonSerialized]
        public string StringData;

        public static IEnumerable<Events> Read()
        {
            return JsonUtility.ReadList<Events>("config/Events.json");
        }

        public Events()
        {

        }

        public Events(Events oldEvent)
        {
            EventName = oldEvent.EventName;
            Comment = oldEvent.Comment;
            Soundname = oldEvent.Soundname;
            Topic = oldEvent.Topic;
            Type = oldEvent.Type;
            Enabled = oldEvent.Enabled;

        }
    }

    [Serializable]
    public class EventsContainer
    {
        public IEnumerable<Events> Events;
    }

    public static class JsonUtility
    {
        public static void WriteFile(string fileName, string data)
        {
            using (StreamWriter r = new StreamWriter(fileName, false))
            {
                r.Write(data);
            }
        }

        public static string ReadFile(string fileName)
        {
            using (StreamReader r = new StreamReader(fileName))
            {
                string json = r.ReadToEnd();
                return json;
            }
        }

        public static T ReadObject<T>(string fileName)
        {
            var s = JsonConvert.DeserializeObject<T>(ReadFile(fileName));
            return s;

            //using (StreamReader r = new StreamReader(fileName))
            //{
            //    string json = r.ReadToEnd();
            //    var s = JsonConvert.DeserializeObject<T>(json);
            //    return s;
            //}
        }



        public static IEnumerable<T> ReadList<T>(string fileName)
        {
            string json = ReadFile(fileName);
            var s = JsonConvert.DeserializeObject<IEnumerable<T>>(json);
            return s;

            //using (StreamReader r = new StreamReader(fileName))
            //{
            //    string json = r.ReadToEnd();


            //    var s = JsonConvert.DeserializeObject<IEnumerable<T>>(json);
            //    return s;
            //}
        }
    }
}